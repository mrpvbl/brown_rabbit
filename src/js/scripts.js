let pageList = new Array() //items on the current page
let currentPage = 1 //the number of the current page
let numberPerPage = 5 //the amount of images to show per page
let numberOfPages = 1 //the total number of pages required to render the collection

window.onload = load


let list
function load(){
    $.ajax({
        url: "content.json",
        dataType: "json",
        success: function(data){
            list = []
            for(x=0; x<data.length; x++){
                list.push(data[x])
            }
            console.log("List array: ")
            console.log(list)

            numberOfPages = getNumberOfPages()
            console.log("Number of pages: "+numberOfPages)

            currentPage = 1
            loadList()

        }
    })
}

function getNumberOfPages(){
    return Math.ceil(list.length / numberPerPage)
}

function nextPage(){
    currentPage += 1
    loadList()
}

function previousPage(){
    currentPage -= 1
    loadList()
}

function firstPage(){
    currentPage = 1
    loadList()
}

function lastPage(){
    currentPage = numberOfPages
    loadList()
}

function loadList(){
    let begin = ((currentPage -1) * numberPerPage)
    let end = begin + numberPerPage
    pageList = list.slice(begin, end)
    drawList()
    check()
}

function drawList(){
    document.getElementById("list").innerHTML = ""
    let template = document.querySelector("#articleTemplate").content;
    for(r=0; r<pageList.length; r++){
        let clone = template.cloneNode(true)
        let img = clone.querySelector("img");
        let title = clone.querySelector("h2");
        let posted = clone.querySelector(".posted");
        let content = clone.querySelector("p");

        img.setAttribute("src", "img/"+pageList[r].url)
        title.textContent = pageList[r].title
        title.classList.add("he"+r)
        posted.textContent = pageList[r].posted
        posted.classList.add("po"+r)
        content.textContent = pageList[r].content
        content.classList.add("co"+r)
        document.getElementById("list").appendChild(clone)

    }
}

function check(){
    document.getElementById("next").disabled = currentPage == numberOfPages ? true : false;
    document.getElementById("previous").disabled = currentPage == 1 ? true : false;
    document.getElementById("first").disabled = currentPage == 1 ? true : false;
    document.getElementById("last").disabled = currentPage == numberOfPages ? true : false;
}
$('#search').keyup(function(){

    for(r=0; r<pageList.length; r++){
        let pageHeading = $('.he'+r)
        let pagePosted = $('.po'+r)
        let pageContent = $('.co'+r)
        searchText(pageHeading)
        searchText(pagePosted)
        searchText(pageContent)
    }

})
function searchText(page){
     let pageText = page.text().replace("<span>","").replace("</span>")
     let searchedText = $('#search').val()
     let theRegEx = new RegExp("("+searchedText+")", "igm")
     let newHtml = pageText.replace(theRegEx ,"<span>$1</span>")
     page.html(newHtml)
}


